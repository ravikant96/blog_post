const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');
const config = require('config');
const URLSearchParams = require('url').URLSearchParams

router.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	next();
});


/* GET home page. */
router.get('/', function (req, res, next) {
	res.render('index', {title: 'Exprexxo'});
});

router.get('/hello', function (req, res, next) {
	res.render('index', {title: 'hello'});
});

router.get('/webSite_detail', async function(req, res, next) {

	const 
		url = config.get('wordpress_url'),
		site_id = config.get('site_id');

	if(!url || !site_id) {
		const err = new Error('Wordpress url not found');
		err.status = 404;
		next(err);
	}
	
	try {
		
		let details = await fetch(
			url + site_id,
			{
				method: "GET"
			}
		);
		
		if(details.status != 200) {
			throw {
				message: "unknown blog",
				status: 400
			}
		}

		details = await details.json();
		
		res.send({
			status: true,
			data: details
		});
	}
	catch(e) {
		
		next(e);
	}

});

router.get('/get_post', async function(req, res, next) {

	const 
		url = config.get('wordpress_url'),
		site_id = config.get('site_id');

	if(!url || !site_id) {
		const err = new Error('Wordpress url not found');
		err.status = 404;
		next(err);
	}

	try {
			
		const parametrs = new URLSearchParams();

		if(req.query.tag) {
			parametrs.set('tag', req.query.tag);
		}

		if(req.query.category) {
			parametrs.set('category', req.query.category);
		}

		let details = await fetch(
			url + site_id + '/posts?' + parametrs.toString(),
			{
				method: "GET"
			}
		);
		
		if(details.status != 200) {
			throw {
				message: "Something went wrong",
				status: 400
			}
		}

		details = await details.json();
		
		res.send({
			status: true,
			data: details
		});
	}
	catch(e) {
		
		next(e);
	}

});

router.get('/get_individual_post_info', async function(req, res, next) {

	const 
		url = config.get('wordpress_url'),
		site_id = config.get('site_id');

	if(!url || !site_id) {
		const err = new Error('Wordpress url not found');
		err.status = 404;
		next(err);
	}

	try {

		if(!req.query.post_id) {

			throw {
				message: "Post id is required",
				status: 400
			}
		}

		let promise = [
			fetch(
				url + site_id + '/posts/' + req.query.post_id,
				{
					method: "GET"
				}
			),
			fetch(
				url + site_id + '/posts' + req.query.post_id + '/related',
				{
					method: "GET"
				}
			)	
		]
		
		const response = await Promise.all(promise);

		let details = response[0];
		let relatedPost = response[1];

		if(details.status != 200 || relatedPost.status != 200) {
			throw {
				message: "Something went wrong",
				status: 400
			}
		}

		details = await details.json();
		relatedPost = await relatedPost.json();
		relatedPost = relatedPost.posts;
		relatedPost.filter( x => x.ID != req.query.post_id);

		const index1 = parseInt(Math.random() * relatedPost.length);
		const index2 = index1 + 1 > relatedPost.length - 1 ? 0 : index1 + 1;
		const index3 = index2 + 1 > relatedPost.length - 1 ? 0 : index2 + 1;

		relatedPost = [relatedPost[index1], relatedPost[index2], relatedPost[index3]];

		res.send({
			status: true,
			data: {details,relatedPost}
		});
	}
	catch(e) {
		next(e);
	}
});

router.get('/categories', async function(req, res, next) {
	
	const 
		url = config.get('wordpress_url'),
		site_id = config.get('site_id');

	if(!url || !site_id) {
		const err = new Error('Wordpress url not found');
		err.status = 404;
		next(err);
	}

	try {

		let details = await fetch(
			url + site_id + '/categories',
			{
				method: "GET"
			}
		);

		if(details.status != 200) {
			throw {
				message: "Something went wrong",
				status: 400
			}
		}

		details = await details.json();
		
		res.send({
			status: true,
			data: details
		});
	}
	catch(e) {
		next(e);
	}

});

router.get('/tags', async function(req, res, next) {
	
	const 
		url = config.get('wordpress_url'),
		site_id = config.get('site_id');

	if(!url || !site_id) {
		const err = new Error('Wordpress url not found');
		err.status = 404;
		next(err);
	}

	try {

		let details = await fetch(
			url + site_id + '/tags',
			{
				method: "GET"
			}
		);

		if(details.status != 200) {
			throw {
				message: "Something went wrong",
				status: 400
			}
		}

		details = await details.json();
		
		if(details.found > 10) {

			details = details.tags.sort((a,b) => a.post_count - b.post_count);

			details = details.splice(0, 10);
		}

		res.send({
			status: true,
			data: details
		});
	}
	catch(e) {
		next(e);
	}

});

module.exports = router;
