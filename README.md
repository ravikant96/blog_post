**About the repository**

A simple blog view application created with the help of javascript, HTML and CSS for the client view and NodeJs for the server side.
The code is written without using any external library.
This web application contains the ability to view the post, filter out the post on the basis of trending categories or tags.

---

## Start the App

Clone the repository.
After cloning follow the steps:-
1. Copy the sample.json file and create a new file with any name. (For further reference I'll use dev.json).

2. Enter the site_id and the port number on which the server would run.

3. Install PM2. (npm install pm2).

4. NODE_ENV="dev" pm2 start bin/www --name blog-post. (Post this command your sever will be up and running).

---

## Prerequisite

HTML, CSS, JavaScript, Node.js

---
