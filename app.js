"use strict";

const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');

const index = require('./server/routes/index');
const client = require('./client/index');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb', parameterLimit: 100000}));
app.use(bodyParser.urlencoded({limit: '50mb', parameterLimit: 100000, extended: true}));
app.use(cookieParser());

app.use('/api/', index);
app.use('/', client);

// catch 404 and forward to error handler
app.use(function (req, res, next) {

	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	if(err.pass) {
		return;
	}
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	console.error('******error request****\n', {
		url: req.originalUrl,
		method: req.method,
		message: err.message,
		status: err.status,
		body: JSON.stringify(req.body),
		query: JSON.stringify(req.query),
		params: JSON.stringify(req.params),
	});

	console.error("******error*****\n", err);

	res.status(err.status || 500);

	res.json({
		status: false,
		message: err.message,
	});
});

module.exports = app;
