"use strict";

const express = require('express');
const router = express.Router();
const config = require('config');

router.use(express.static('./client'));

class HTMLAPI  {

	constructor() {
		
		this.stylesheets = [
			'/css/main.css',
			'https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous" f="'
		];

		this.scripts = [
			'/js/main.js',
		];
	}

	async body() {

		return `<!DOCTYPE html>
			<html lang="en">
				<head>
					<meta name="viewport" content="width=device-width, initial-scale=1">
					<title></title>

					${this.stylesheets.map(s => '<link rel="stylesheet" type="text/css" href="' + s + '">\n\t\t\t\t\t').join('')}
					${this.scripts.map(s => '<script src="' + s + '"></script>\n\t\t\t\t\t').join('')}
				</head>
				<body>

					<div class="loader-container">
						<div class="loader">
							<div></div>
							<div></div>
							<div></div>
						</div>
					</div>

					<main>
						${this.main ? await this.main() || '' : ''}
					</main>
				</body>
			</html>
		`;
	}
}

const serveFrontEnd = function (endpoint) {

	return async function(request, response, next) {
		
		const _endPoint = new endpoint();

		response.send(await _endPoint.body());
	}
}

router.get('/home', serveFrontEnd(class Home extends HTMLAPI {

	constructor() {
		
		super();

		this.stylesheets.push('/css/home.css');
		this.scripts.push('/js/home.js');
	}
}));

router.get('/post/:id?', serveFrontEnd(class Post extends HTMLAPI {

	constructor() {

		super();

		this.stylesheets.push('/css/home.css');
		this.stylesheets.push('/css/post.css');
		this.scripts.push('/js/post.js');
	}
}));

module.exports = router;