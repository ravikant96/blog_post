
if(typeof window != 'undefined') {
    
    window.addEventListener('DOMContentLoaded', async () => {

		await Page.setup();

		if(!Page.class) {
			return;
		}

		window.page = new (Page.class)();
	});
}

class Page {

	static async setup() {

		AJAXLoader.setup();

		await Page.load();
	}

	static async load() {

		// SnackBar.setup();
	}

	constructor() {

		this.container = document.querySelector('main');

        this.urlSearchParameters = new URLSearchParams(location.search);

        this.header = new PageHeader(this);

		this.renderPage();
    }
    
	renderPage() {

		document.body.insertBefore(this.header.container, this.container);
	}
}

class PageHeader {

	constructor(page) {

		this.page = page;
	}

	get container() {

		if(this.containerElement)
			return this.containerElement;

        const container = this.containerElement = document.createElement('header');
        
		container.innerHTML = `

			<span class="burger-icon"><i class="fas fa-bars"></i></span>

            <a class="logo" href="/home"><img src="https://truecaller.blog/wp-content/uploads/2017/04/truecaller-logo-white-e21aa38-e1491987638993.png"></a>
            
            <span class="item">
                <a href="http://www.truecaller.com/" target="_blank">Visit our site</a>
            </span>
		`;

		container.querySelector('.burger-icon').on('click', (e) => {

			e.stopPropagation();
			
			if(document.querySelector('.side-bar')) {
				document.querySelector('.side-bar').classList.toggle('slide');
			}
		});

		document.body.on('click', () => {
			
			if(document.querySelector('.side-bar')) {
				document.querySelector('.side-bar').classList.remove('slide');
			}
		})

		return container;
	}
}

Page.exception = class PageException extends Error {

	constructor(message) {

		super(message);
	}
}

class API {

	static async call(url, _parameters = {}, options = {}) {

		AJAXLoader.show();
        
        url = '/api' + url;

		const parameters = new URLSearchParams(_parameters);

		if(options.method == 'POST') {

			options.body = parameters.toString();

			options.headers = {
				'Content-Type': 'application/x-www-form-urlencoded',
			};
		}

		else if(_parameters)
			url += '?' + parameters.toString();

		let response = null;

		try {
			response = await fetch(url, options);
		}
		catch(e) {
			AJAXLoader.hide();
			throw new API.Exception(e, 'API Execution Failed');
		}

        const result =  response.headers.get('content-type').includes('json') ? await response.json() : await response.text();
        
        AJAXLoader.hide();

        if(!result.status) {
            throw new API.Exception(e, 'API Execution Failed');
        }

        return result.data;
	}
}

API.Exception = class {

	constructor(response = {}) {

		this.status = response.status || '';
		this.message = response.message || '';
		this.body = response;
	}
}

class AJAXLoader {

	static setup() {
        document.querySelector('.loader-container').classList.add('hidden');
	}

	/**
	 * Show the working flag.
	 */
	static show() {

		if(typeof document == 'undefined')
            return;
		
		document.body.style.overflow = 'hidden';

        document.querySelector('.loader-container').classList.add('show');
        document.querySelector('.loader-container').classList.remove('hidden');
	}

	/**
	 * Hide the flag.
	 */
	static hide() {

		if(typeof document == 'undefined')
            return;
		
		document.body.style.overflow = 'unset';
			
        document.querySelector('.loader-container').classList.remove('show');
        document.querySelector('.loader-container').classList.add('hidden');
	}
}

class Format {

	static ago(timestamp) {

		if(!timestamp)
			return '';

		const
			currentSeconds = typeof timestamp == 'number' ? timestamp : Date.parse(timestamp),
			agoFormat = [
				{
					unit: 60,
					minimum: 5,
					name: 'second',
				},
				{
					unit: 60,
					minimum: 1,
					name: 'minute',
				},
				{
					unit: 24,
					minimum: 1,
					name: 'hour',
					prefix: 'An',
				},
				{
					unit: 7,
					minimum: 1,
					name: 'day',
				},
				{
					unit: 4.3,
					minimum: 1,
					name: 'week',
				},
				{
					unit: 12,
					minimum: 1,
					name: 'month',
				},
				{
					name: 'year',
				},
			];

		//If the time is future.
		if(currentSeconds > Date.now())
			return '';

		//If the date is invalid;
		if(!currentSeconds)
			return 'Invalid Date';

		let
			time = Math.floor((Date.now() - currentSeconds) / 1000),
			finalString = '',
			format = agoFormat[0];

		for(const data of agoFormat) {

			//If the time format is year then break.
			if(agoFormat.indexOf(data) >= agoFormat.length - 1)
				break;

			format = data;

			format.time = time;

			time = Math.floor(time / format.unit);

			if(!time)
				break;
		}

		//Special case for year.
		const years = time % 12;

		if(years) {

			finalString = years == 1 ? 'A year ago' : Format.dateTime(timestamp);
		}
		else
			finalString = calculateAgo(format);


		function calculateAgo(format) {

			const
				range = format.unit - (0.15 * format.unit),
				time = format.time % format.unit,
				index = agoFormat.indexOf(format);

			let string = `${time} ${format.name}s ago`;

			if(time <= format.minimum)
				string = format.name.includes('second') ? 'Just Now' : `${format.prefix || 'A'} ${format.name} ago`;
			else if(time >= range) {

				let
					nextFormat = agoFormat[index + 1],
					prefix = nextFormat.prefix || 'a';

				string = `About ${prefix.toLowerCase()} ${nextFormat.name} ago`;
			}

			return string;
		}

		return finalString;
	}

	static date(date) {

		const options = {
			year: 'numeric',
			month: 'short',
			day: 'numeric',
			timeZone: 'UTC',
		};

		if(!Format.date.formatter)
			Format.date.formatter = new Intl.DateTimeFormat(page.urlSearchParameters.get('locale') || undefined, options);

		if(typeof date == 'string')
			date = Date.parse(date);

		if(typeof date == 'object' && date)
			date = date.getTime();

		if(!date)
			return '';

		return Format.date.formatter.format(date);
	}

	static month(month) {

		const options = {
			year: 'numeric',
			month: 'short',
			timeZone: 'UTC',
		};

		if(!Format.month.formatter)
			Format.month.formatter = new Intl.DateTimeFormat(page.urlSearchParameters.get('locale') || undefined, options);

		if(typeof month == 'string')
			month = Date.parse(month);

		if(typeof month == 'object' && month)
			month = month.getTime();

		if(!month)
			return '';

		return Format.month.formatter.format(month);
	}

	static year(year) {

		const options = {
			year: 'numeric',
			timeZone: 'UTC',
		};

		if(!Format.year.formatter)
			Format.year.formatter = new Intl.DateTimeFormat(page.urlSearchParameters.get('locale') || undefined, options);

		if(typeof year == 'string')
			year = Date.parse(year);

		if(typeof year == 'object' && year)
			year = year.getTime();

		if(!year)
			return '';

		return Format.year.formatter.format(year);
	}

	static time(time) {

		const options = {
			hour: 'numeric',
			minute: 'numeric'
		};

		if(!Format.time.formatter)
			Format.time.formatter = new Intl.DateTimeFormat(page.urlSearchParameters.get('locale') || undefined, options);

		if(typeof time == 'string')
			time = Date.parse(time);

		if(typeof time == 'object' && time)
			time = time.getTime();

		if(!time)
			return '';

		return Format.time.formatter.format(time);
	}

	static customTime(time, format) {

		if(!Format.cachedFormat)
			Format.cachedFormat = [];

		let selectedFormat;

		for(const data of Format.cachedFormat) {

			if(JSON.stringify(data.format) == JSON.stringify(format))
				selectedFormat = data;
		}

		if(!selectedFormat) {

			selectedFormat = {
				format: format,
				formatter: new Intl.DateTimeFormat(page.urlSearchParameters.get('locale') || undefined, format),
			};

			Format.cachedFormat.push(selectedFormat);
		}

		Format.customTime.formatter = selectedFormat.formatter;

		if(time && typeof time == 'string')
			time = Date.parse(time);

		if(time && typeof time == 'object')
			time = time.getTime();

		if(!time)
			return '';

		return Format.customTime.formatter.format(time);
	}

	static dateTime(dateTime) {

		const options = {
			year: 'numeric',
			month: 'short',
			day: 'numeric',
			hour: 'numeric',
			minute: 'numeric'
		};

		if(!Format.dateTime.formatter)
			Format.dateTime.formatter = new Intl.DateTimeFormat(page.urlSearchParameters.get('locale') || undefined, options);

		if(typeof dateTime == 'string')
			dateTime = Date.parse(dateTime);

		if(typeof dateTime == 'object' && dateTime)
			dateTime = dateTime.getTime();

		if(!dateTime)
			return '';

		return Format.dateTime.formatter.format(dateTime);
	}
}

if(typeof Node != 'undefined') {
	Node.prototype.on = window.on = function(name, fn) {
		this.addEventListener(name, fn);
	}
}