class PostDetail extends Page {

    constructor() {
        
        super();
        
        this.post_id = location.href.split('/').pop().split('?')[0];

        if(!this.post_id) {
            location.href = '/home';
        }

        this.load();
    }

    async load() {

        const parameter = {
            post_id: this.post_id
        }

        const data = await API.call('/get_individual_post_info', parameter);

        Object.assign(this, data.details);
        this.relatedPosts = new RelatedPosts(data.relatedPost, this);

        this.render();
    }

    get section() {

        if(this.sectionElement) {
            return this.sectionElement;
        }

        const container = this.sectionElement = document.createElement('section');

        container.innerHTML = `
            <div class="post-detail"></div>
            <div class="related-post"></div>
        `;

        return container;
    }

    render() {

        this.section.querySelector('.post-detail').appendChild(this.detailedContainer);
        this.section.querySelector('.related-post').appendChild(this.relatedPosts.container);
        this.container.appendChild(this.section);
    }

    get detailedContainer() {

        if(this.detailedContainerElement) {
            return this.detailedContainerElement;
        }

        const container = this.detailedContainerElement = document.createElement('div');
        container.classList.add('details');

        container.innerHTML = `
            <figure>
                <img class="hidden hide-opacity" height="100%" width="100%" alt="${this.slug}">
                <div class="post-loader-container"><div class="post-loader"><div></div><div></div><div></div><div></div></div></div>
                <span class="NA hidden">Preview not available!</span>
            </figure>

            <div class="tags"></div>
            <h1 class="heading">${this.title}</h1>
            <div class="time"><i class="far fa-clock"></i>${Format.ago(this.date)}</div>
            <div class="content">${this.content}</div>
        `;

        const
            img = container.querySelector('figure img'),
            loader = container.querySelector('figure .post-loader-container'),
            NA = container.querySelector('figure .NA');

        img.on('load', () => {

            img.classList.remove('hidden');
            img.classList.remove('hide-opacity');
            loader.classList.add('hidden');
        });

        img.on('error', () => {
            NA.classList.remove('hidden');
            loader.classList.add('hidden');
        });

        img.src = this.post_thumbnail.URL;

        return container;
    }
}

Page.class = PostDetail;

class RelatedPosts extends Set {
    
    constructor(related, page) {
        super();
        this.page = page;

        for(const post of related) {
            this.add(new Related(post))
        }
    }

    get container() {

        if(this.containerElement) {
            return this.containerElement;
        }

        const container = this.containerElement = document.createElement('div');
        container.innerHTML = `
            <h3>Related Posts</h3>
            <div class="rel-post"></div>
        `;

        this.render();

        return container;
    }

    render() {
        
        const container = this.container.querySelector('.rel-post');
        container.textContent = null;

        for(const post of this) {
            container.appendChild(post.container);
        }
    }
}

class Related {

    constructor(post) {

        Object.assign(this, post);
    }

    get container() {
        
        if(this.containerElement) {
            return this.containerElement;
        }

        const container = this.containerElement = document.createElement('article');
        container.innerHTML = `
            <figure>
                <img class="hidden hide-opacity" height="100%" width="100%" alt="${this.slug}">
                <div class="post-loader-container"><div class="post-loader"><div></div><div></div><div></div><div></div></div></div>
                <span class="NA hidden">Preview not available!</span>
            </figure>
            <h4 class="heading">${this.title}</h4>
        `;
        const
            img = container.querySelector('figure img'),
            loader = container.querySelector('figure .post-loader-container'),
            NA = container.querySelector('figure .NA');

        img.on('load', () => {

            img.classList.remove('hidden');
            img.classList.remove('hide-opacity');
            loader.classList.add('hidden');
        });

        img.on('error', () => {
            NA.classList.remove('hidden');
            loader.classList.add('hidden');
        });

        img.src = this.post_thumbnail.URL;

        container.on('click', () => {
            location.href = '/post/' + this.ID;
        });

        return container;
    }
}