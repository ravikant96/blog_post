class Home extends Page {

    constructor() {

        super();
        
        window.on('popstate', e => this.loadState(e.state));

        (async () => {

			await this.load();

			this.loadState();
		})();
    }

    loadState(state) {
        
        let what = state ? state.what : location.pathname.split('/');

		what = what.pop();

		if(!parseInt(what)) {
            
            history.pushState(null, '', `/home`);

            return;
        }

        const post = this.list.get(what);

        post.render();
    }

    async load() {
        
        await this.fetch();

        this.render();
    }

    async fetch() {

        this.post = new Posts(this);
        this.topTags = new TopTags(this);
        this.categories = new Categories(this);

        //load post details
        await this.post.load();
        await this.topTags.load();
        await this.categories.load();
    }

    get section() {

        if(this.sectionElement) {
            return this.sectionElement;
        }

        const container = this.sectionElement = document.createElement('section');
        container.classList.add('section-container');

        container.innerHTML = `
            <nav class="side-bar"></nav>
            <div class="post-container"></div>
        `;

        return container;
    }

    render() {

        this.container.appendChild(this.section);

        const postContainer = this.section.querySelector('.post-container');
        postContainer.appendChild(this.post.container);

        this.section.querySelector('.side-bar').appendChild(this.categories.container);
        this.section.querySelector('.side-bar').appendChild(this.topTags.container);
    }
}

Page.class = Home;

class Posts extends Map {

    constructor(page) {

        super();

        this.page = page;
    }

    async load({tag = "", category = ""} = {}) {

        const parameters = {};

        if(tag) {
            parameters.tag = tag;
        }

        if(category) {
            parameters.category = category;
        }

        const result = await API.call('/get_post', parameters);

        this.clear();

        for(const item of result.posts) {
            this.set(item.ID, new Post(item, this));
        }

        this.render();
    }

    get container() {

        if(this.containerElement) {
            return this.containerElement;
        }

        const container = this.containerElement = document.createElement('div');
        container.classList.add('posts');

        return container;
    }

    render() {

        this.container.innerHTML = null;

        for(const post of this.values()) {
            this.container.appendChild(post.thumbnail);
        }

        if(!this.size) {
            this.container.innerHTML = '<span class="NA">No related posts found</span>';
        }
    }
}

class Post {

    constructor(post, page) {

        this.page = page;
        Object.assign(this, post);

        this.tags = new Tags(Object.values(this.categories), this)
    }

    get thumbnail() {

        if(this.thumbnailContainer) {
            return this.thumbnailContainer;
        }

        const container = this.thumbnailContainer = document.createElement('article');
        container.classList.add('post-thumbnail', this.ID);

        container.innerHTML = `
            <figure>
                <img class="hidden hide-opacity" height="100%" width="100%" alt="${this.slug}">
                <div class="post-loader-container"><div class="post-loader"><div></div><div></div><div></div><div></div></div></div>
                <span class="NA hidden">Preview not available!</span>
            </figure>

            <div class="tags"></div>
            <h1 class="heading">${this.title}</h1>
            <div class="time"><i class="far fa-clock"></i>${Format.ago(this.date)}</div>
            <div class="excerpt">${this.excerpt}</div>
            <div class="continue">
                <a href="/post/${this.ID}" class="continue-reading">Continue reading...</a>
            </div>
        `;

        const
            img = container.querySelector('figure img'),
            loader = container.querySelector('figure .post-loader-container'),
            NA = container.querySelector('figure .NA');

        img.on('load', () => {

            img.classList.remove('hidden');
            img.classList.remove('hide-opacity');
            loader.classList.add('hidden');
        });

        img.on('error', () => {
            NA.classList.remove('hidden');
            loader.classList.add('hidden');
        });

        img.on('click', () => {

            location.href = '/post/' + this.ID;
        });

        img.src = this.post_thumbnail.URL;
        
        container.querySelector('.tags').appendChild(this.tags.container);

        return container;
    }

    get detailedContent() {

    }

    render() {

    }
}

class TopTags {
    
    constructor(page) {

        this.page = page;
    }

    async load() {

        const tags = await API.call('/tags');
        this.tags = new Tags(tags, this);
    }

    get container() {

        if(this.containerElement) {
            return this.containerElement;
        }
        
        const container = this.containerElement = document.createElement('div');
        container.classList.add('tags-list');

        container.innerHTML = `
            <h3> Top Trending Tags</h3>
        `;

        container.appendChild(this.tags.container);

        return container;
    }
}

class Categories extends Set {
    
    constructor(page) {

        super();
        this.page = page;
    }

    async load() {

        const categories = await API.call('/categories');

        let i = 0;

        for(const cat of categories.categories) {

            if(i > 5) {
                break;
            }

            this.add(new Category(cat, this));
            
            i++;
        }
    }
    get container() {

        if(this.containerElement) {
            return this.containerElement;
        }
        
        const container = this.containerElement = document.createElement('div');
        container.classList.add('cat-list');

        container.innerHTML = `
            <h3> Top Trending Categories</h3>
        `;

        for(const category of this) {
            container.appendChild(category.container);
        }

        return container;
    }
}

class Category {

    constructor(cat, page) {

        this.page = page;

        Object.assign(this, cat);
    }

    get container() {

        if(this.containerElement) {
            return this.containerElement;
        }

        const container = this.containerElement = document.createElement('span');
        container.classList.add('category');

        container.innerHTML = this.name;

        container.on('click', () => {

            if(this.page.page.post) {
                this.page.page.post.load({category: this.slug});
            }
            else {
                this.page.page.load({category: this.slug})
            }
        });

        return container;
    }
}

class Tags {

    constructor(tags, page) {

        this.list = [];

        for(const tag of tags) {
            this.list.push(new Tag(tag, this));
        }

        this.page = page;
    }

    get container() {

        if(this.containerElement) {
            return this.containerElement;
        }

        const container = this.containerElement =  document.createElement('div');
        container.classList.add('tags');

        for(const element of this.list) {
            container.appendChild(element.container);
        }

        return container;
    }
}

class Tag {

    constructor(tag, page) {

        Object.assign(this, tag);
        this.tags = page;
    }

    get container() {

        if(this.containerElement) {
            return this.containerElement;
        }

        const container = this.containerElement = document.createElement('span');
        container.classList.add('tag');

        if(this.name.toLowerCase().includes('danger') || this.name.toLowerCase().includes('alert')) {
            container.classList.add('red');
        }
        else if(this.name.toLowerCase().includes('safe') || this.name.toLowerCase().includes('news')) {
            container.classList.add('green');
        }
        else {
            container.classList.add('default');
        }
        
        container.innerHTML = `
            ${this.name}
        `;

        container.on('click', () => {

            if(this.tags.page.page.post) {
                this.tags.page.page.post.load({tag: this.slug})
            }
            else {
                this.tags.page.page.load({tag: this.slug})
            }
        })

        return container;
    }
}